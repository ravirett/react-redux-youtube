// imports
import _ from 'lodash';
import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import SearchBar from './components/searchBar';
import VideoList from './components/videoList';
import VideoPlayer from './components/videoPlayer';
import YTSearch from 'youtube-api-search';

const YT_API_KEY = 'AIzaSyB_4q_7iWW92sXNsI_DAGEZpu7VRC7oAOg';
const INIT_SEARCH_TERM = 'geek sundry tabletop';

class App extends Component {
    constructor(props) {
        super(props);

        this.state = {
            videos: [],
            selectedVideo: null
        };

        this.doSearch(INIT_SEARCH_TERM);
    }

    doSearch(term) {
        YTSearch({key: YT_API_KEY, term: term}, (videos) => {
            this.setState({videos: videos, selectedVideo: videos[0]});
        });
    }

    render() {
        const debouncedSearch = _.debounce((term) => {this.doSearch(term)}, 1000);

        return (
            <div>
                Hi
                <SearchBar handleSearch={debouncedSearch}/>
                <VideoPlayer video={this.state.selectedVideo}/>
                <VideoList videos={this.state.videos} handleVideoSelect={selectedVideo => this.setState({selectedVideo})}/>
            </div>
        );
    }
}

ReactDOM.render(<App />, document.querySelector('.container'));