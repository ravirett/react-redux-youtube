import React from 'react';
import VideoItem from './videoItem';

const VideoList = (props) => {
    const VideoItems = props.videos.map((video, index) => {
       return <VideoItem handleVideoClick={props.handleVideoSelect} video={video} key={video.id.videoId}/>
    });

    return (
        <ul className="col-md-4 list-group">
            {VideoItems}
        </ul>
    )
}

export default VideoList;