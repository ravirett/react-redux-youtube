import React, {Component} from 'react';

class SearchBar extends Component {
    constructor(props) {
        super(props);

        this.state = {
            term: ''
        };
    }

    handleInputChange(term) {
        this.setState({term});
        this.props.handleSearch(term);
    }

    render() {
        return (
            <div className="search-bar">
                <input
                    className="search-input"
                    value={this.state.term}
                    onChange={(e) => this.handleInputChange(e.target.value)}
                    ref="searchInput"
                />
            </div>
        );
    }
}

export default SearchBar;